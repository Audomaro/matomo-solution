# Matomo

Matomo, anteriormente Piwik (pronunciado / ˈpiːwiːk /), es una aplicación de análisis web de código abierto y gratuita desarrollada por un equipo de desarrolladores internacionales, que se ejecuta en un servidor web PHP / MySQL. Realiza un seguimiento de las visitas en línea a uno o más sitios web y muestra informes sobre estas visitas para su análisis. En junio de 2018, más de 1.455.000 sitios web utilizaban Matomo, o el 1,3% de todos los sitios web con herramientas de análisis de tráfico conocidas, y se ha traducido a 54 idiomas  Las nuevas versiones se lanzan con regularidad.

## Requisitos

Para ejecutar esta solución se requiere tener docker en su versión más reciente, así como tener una cuenta en `docker hub` y habilitar la opción `WSL 2`.

> **Nota**: No es necesario habilitar la opción `Send usage statistics` en docker.

## Componentes de la solución

Este proyecto de dockerización se compone por tres servicios docker (todas son imágenes oficiales):

| Servicio | Descripción                                                                                                                      | Sitio                              | Docker                             | Versión exacta usada |
|----------|----------------------------------------------------------------------------------------------------------------------------------|------------------------------------|------------------------------------|----------------------|
| mariadb  | Sistema de base de datos derivado de `mysql`.                                                                                    | <https://hub.docker.com/_/mariadb> | <https://hub.docker.com/_/mariadb> | mariadb:10.5.9       |
| matomo   | Aplicación para analisis web.                                                                                                    | <https://matomo.org>               | <https://hub.docker.com/_/matomo>  | matomo:fpm-alpine    |
| nginx    | Nginx es un servidor web que también puede ser usado como proxy inverso, balanceador de carga y proxy para protocolos de correo. | <https://www.nginx.com>            | <https://hub.docker.com/_/nginx>   | nginx:1.19.8-alpine  |

> **NOTA**: una version de docker con tag `alpine` hace referencia a que usa como sistema base `Alpine Linux` que es una distribución minimalista basada en `busybox` y `musl-libc` hace que las imágenes ocupen bastante menos que una imagen equivalente, por ejemplo, basada en Ubuntu o Debian.

## Configuración de los servicios

La configuración generales del docker se encuentra en el archivo `docker-compose.yml` y las configuraciones especificas para cada servicio se encuentra en el folder `config`; estas ultimas se mantienen por separado por si se desea modificarlas no sea necesario realizar cambios en el archivo de docker.

Archivos de la carpeta `config`:

- `db.env`: configuración relacionada a al sistema de base de datos de `mariadb` y la pre-configuración de base de datos para `matomo`.
- `setup.sql`: script sql para ejecutar al crear al crear el servicio`db`, crea la base datos `matomo` y el usuario `matomo` junto con sus accesos para ejecutar comandos desde dentro o fuera del equipo; `matomo@'%'`.
- `matomo.conf`: contiene la configuración para que la aplicación `matomo` sea hospedada en `nginx`.
- `ssl.conf`: contiene la configuración para que `nginx` se ejecute sobre le protocolo `https`; en este caso no se cargo esta configuración con `docker-compose.yml`.

## Uso de la solución

Es importante posicionar en la ruta del repositorio con la `terminal`/`shell` para ejecutar los comandos.

Para crear e iniciarlo:

```bash
docker-compose up -d
```

Para detener 👁️‍:

```bash
docker-compose stop
```

Para iniciar 👁️‍:

```bash
docker-compose start
```

Para reiniciar 👁️‍:

```bash
docker-compose restart
```

Para eliminar 👁️‍; la opción `-v` permite eliminar los `volumes` creados:

```bash
docker-compose down -v
```

: 👁️‍ también se puede realizar esta acción desde la interfaz gráfica de docker.

## Configuración de `matomo`

Cuando se inicie por primera vez se debe entrar al sitio de la aplicación el cual es: <http://ip_de_tu_maquina:8080>; evitar usar el `http://localhost`.

Una vez dentro del sitio, Pasos:

- Pasos del 1 al 4. Configuraciones de base de datos: aquí es siguiente, siguiente y es porque se pre-configuro con el archivo `db.env`.
- Paso 5 - Super Usuario: es la creación de usuario para acceder a la aplicación, ejemplo:
  - nombre: **matomo**
  - contraseña: **p@55w0rd**
  - email: **matomo@fake-email.com**
- Paso 6 - Configurar un sitio de internet; puede ser uno ficticio.
- Paso 7 - Código de monitoreo JavaScript: este es el código javascript que se debe agregar al sitio al que deseemos rastrear.
- Paso 8 - Enhorabuena: Paso solo informativo.

## Configuración extra

Una vez configurado `matomo` nos redireccionara al sitio pero saldra un warning debido aque estamos accediendo por el `puerto 8080` y matomo esta pre-configurado para el `puerto 80`, para solucionarlo hay que editar el archivo mencionado y agregar la configuración con nuestra ip, por ejemplo:

```any
How do I fix this problem and how do I login again?
The Matomo Super User can manually edit the file piwik/config/config.ini.php and add the following lines:

[General]
trusted_hosts[] = "192.168.1.80:8080"
```

Pasos para solucionar este detalle:

- Ir a la ruta del archivo: `\\wsl$\docker-desktop-data\version-pack-data\community\docker\volumes\matomo-solution_matomo\_data\config`
  - En este caso se esta tomando como base que se habilito la opcion de `WSL 2` en docker
- Editar el archivo `config.ini.php` y cambiar la propiedad `trusted_hosts`
  - En mi caso la linea paso de `trusted_hosts[] = "192.168.1.80"` a verse como `trusted_hosts[] = "192.168.1.80:8080"`
- `F5` e iniciar sesión.
